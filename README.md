kodus/error-handler
===================

This package implements an error-handler specifically for error-handling during web-requests
to a [PSR-15](https://github.com/http-interop/http-middleware) middleware dispatcher.

This is different from your typical *global* error-handler, and [we don't think you need one](#no-global).

### Overview

This package ships with two error-handler implementations:

 * `DebugErrorHandler` for use during development:
   * Improves readability of XDebug output. (rather than replacing it.)
   * Uses a fall-back on systems without XDebug.
   * Emits detailed stack-traces for all exceptions, errors, warning and notices.

 * `FriendlyErrorHandler` for use in production:
   * Hides the details of all exceptions and errors.
   * A basic "apology screen" is the only user-facing output.
   * Logs detailed stack-traces to a [PSR-3](http://www.php-fig.org/psr/psr-3/) logger.
   * Tolerates and logs warnings and notices.

To customize the user-facing "apology screen", simply extend `FriendlyErrorHandler` and
override the protected `createErrorPage()` method.

Note that both implementations respect the `Accept` header and will emit either HTML,
JSON or plain-text output.

### Usage

Use any PSR-15 middleware dispatcher and insert an `ErrorHandlerMiddleware` instance as the
first component in your middleware-stack.

For example, using [Middleman](https://github.com/mindplay-dk/middleman) and the
`DebugErrorHandler` with [Diactoros](https://github.com/zendframework/zend-diactoros) as
your [PSR-7](http://www.php-fig.org/psr/psr-7/) implementation:

```php
$handler = new DebugErrorHandler(new DiactorosResponseFactory());

$dispatcher = new Dispatcher([
    new ErrorHandlerMiddleware($handler),
    new YourMiddleware(...),
    ...
]);
```

If you're using a ([PSR-11](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-11-container.md))
DI container, you will most likely want to register one of the two error-handlers under the `ErrorHandler`
interface and inject it into the `ErrorHandlerMiddleware` constructor.

<a name="no-global"></a>

### What? No Global Error-Handling?

If you're using a PSR-15 middleware dispatcher in your front-controller, an application
with an error-handler at the *top* of the middleware-stack is practically unbreakable. 

Something *could* theoretically go wrong before the middleware-stack gets dispatched, but
it's very unlikely to happen. In cases where the core bootstrapping of your application
is broken, it's just as likely your global error-handler didn't get bootstrapped in the
first place - and it's highly unlikely you'd end up deploying a completely broken project
to production, so we don't think global error-handling is necessary.

We've also found that global error-handlers interfere with the conditions under test, as
test-frameworks generally have their own built-in error-handling.

As for console-applications, most frameworks (such as `symfony/console`) also have their
own built-in error-handling, and again, a global error-handler could interfere.
 
And as for plain PHP scripts, the default error-handling is usually enough - we wouldn't
like to trust a global error-handler for automated scripts to send e-mail notifications,
for example; it's generally safer to let a cron-tasks exit with a failure status-code
and use a language-independent facility to notify a system administrator.

This is our *opinion* on which the design of this package was informed.

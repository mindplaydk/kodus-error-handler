<?php

namespace Kodus\Error;

use ErrorException;
use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use RuntimeException;
use Throwable;

/**
 * This middleware component should be placed at the top of the middleware-stack, where
 * it will delegate to the rest of the middleware-stack while handling `Throwable` errors.
 */
class ErrorHandlerMiddleware implements MiddlewareInterface
{
    /**
     * @var ErrorHandler
     */
    private $handler;

    /**
     * @param ErrorHandler $handler
     */
    public function __construct(ErrorHandler $handler)
    {
        $this->handler = $handler;
    }

    public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        set_error_handler(function ($severity, $message, $filename, $line) {
            $error = new ErrorException($message, 0, $severity, $filename, $line);

            if (error_reporting() !== 0) {
                // NOTE: if error_reporting() is 0, that means the error-suppression operator was used.
                //       well, probably. hopefully. oh my, PHP.

                $this->handler->handleError($error);
            }
        });

        $ob_level = ob_get_level();

        try {
            $response = $delegate->process($request);

            if (ob_get_level() !== $ob_level) {
                throw new RuntimeException("unexpected buffer-level: " . ob_get_level() . ", expected: {$ob_level}");
            }
        } catch (Throwable $error) {
            $response = $this->handler->createErrorResponse($request, $error);
        } finally {
            restore_error_handler();

            while (ob_get_level() > $ob_level) {
                ob_end_clean(); // clean any hanging output buffers
            }
        }

        return $response;
    }
}

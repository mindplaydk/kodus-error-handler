<?php

namespace Kodus\Error;

/**
 * MIME-type constants
 */
abstract class MIME
{
    const HTML = "text/html";
    const JSON = "application/json";
    const TEXT = "text/plain";
}

<?php

namespace Kodus\Error;

use Psr\Http\Message\ResponseInterface;

/**
 * TODO replace this abstraction with the PSR HTTP Factory
 *
 * @link https://github.com/http-interop/http-factory
 */
interface ResponseFactory
{
    /**
     * @param int $status HTTP status code
     *
     * @return ResponseInterface
     */
    public function createResponse($status = 200): ResponseInterface;
}

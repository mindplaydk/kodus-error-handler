<?php

namespace Kodus\Error;

use Kodus\Error\View\ErrorPage;
use Negotiation\Accept;
use Negotiation\Negotiator;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Throwable;

/**
 * Abstract implementation of the ErrorHandler interface, capable of selecting
 * the appropriate Response MIME-type, and defines abstract methods for creation
 * of the HTML ErrorPage, a JSON payload, or a plain-text Response.
 */
abstract class AbstractErrorHandler implements ErrorHandler
{
    /**
     * @var ResponseFactory
     */
    private $factory;

    public function __construct(ResponseFactory $factory)
    {
        $this->factory = $factory;
    }

    public function createErrorResponse(
        ServerRequestInterface $request,
        Throwable $error
    ): ResponseInterface {
        $response_type = $this->getResponseType($request);

        if ($response_type === MIME::HTML) {
            return $this->createHTMLResponse($this->createErrorPage($request, $error));
        }

        if ($response_type === MIME::JSON) {
            return $this->createJSONResponse($this->createJSONPayload($request, $error));
        }

        return $this->createTextResponse($this->createPlainText($request, $error));
    }

    protected function createHTMLResponse(string $html): ResponseInterface
    {
        $response = $this->createResponse("text/html; charset=utf-8");

        $response->getBody()->write($html);

        return $response;
    }

    protected function createJSONResponse(array $data): ResponseInterface
    {
        $response = $this->createResponse("application/json");

        $body = json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);

        $response->getBody()->write($body);

        return $response;
    }

    protected function createTextResponse(string $text): ResponseInterface
    {
        $response = $this->createResponse("text/plain");

        $response->getBody()->write($text);

        return $response;
    }

    abstract protected function createErrorPage(ServerRequestInterface $request, Throwable $error): string;

    abstract protected function createJSONPayload(ServerRequestInterface $request, Throwable $error): array;

    abstract protected function createPlainText(ServerRequestInterface $request, Throwable $error): string;

    private function createResponse(string $content_type): ResponseInterface
    {
        return $this->factory
            ->createResponse(500)
            ->withHeader("Content-Type", $content_type);
    }

    /**
     * Internally negotiate the `Content-Type` preferred by the client.
     *
     * @see MIME
     *
     * @param ServerRequestInterface $request
     *
     * @return string MIME content-type
     */
    private function getResponseType(ServerRequestInterface $request): string
    {
        if (! $request->hasHeader("Accept")) {
            return MIME::TEXT;
        }

        $negotiator = new Negotiator();

        /** @var Accept $result */
        $result = $negotiator->getBest(
            $request->getHeaderLine("Accept"),
            [MIME::HTML, MIME::JSON, MIME::TEXT]
        );

        return $result
            ? $result->getValue()
            : MIME::TEXT;
    }
}

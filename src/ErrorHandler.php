<?php

namespace Kodus\Error;

use ErrorException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Throwable;

/**
 * This interface defines error-handling behavior and the creation of error-responses.
 *
 * @see ErrorHandlerMiddleware
 */
interface ErrorHandler
{
    /**
     * This method defines the behavior when an E_WARNING, E_NOTICE, E_STRICT (etc.) has been caught.
     *
     * If the implementation chooses to `throw` the `ErrorException`, it will bubble to the
     * {@see createErrorResponse} method for the creation of an error-response.
     *
     * @param ErrorException $error
     */
    public function handleError(ErrorException $error);

    /**
     * This method implements the creation of an error-response for a `Throwable` when caught by
     * the error-handler middleware.
     *
     * @param ServerRequestInterface $request the Request during which the error was caught
     * @param Throwable              $error   the error being reported
     *
     * @return ResponseInterface
     */
    public function createErrorResponse(
        ServerRequestInterface $request,
        Throwable $error
    ): ResponseInterface;
}

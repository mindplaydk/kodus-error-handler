<?php

namespace Kodus\Error;

use ErrorException;
use Kodus\Error\View\ErrorPage;
use Kodus\Error\View\FriendlyErrorPage;
use mindplay\readable;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Throwable;

/**
 * This error-handler is intended for use in production.
 *
 * On fatal error, detailed file/line-number and stack-trace information is
 * written to a PSR-3 logger, hidden from user-facing output, and produces only
 * a user-facing "friendly apology" error-page.
 *
 * Non-fatal errors (such as E_WARNING, E_NOTICE, etc.) are tolerated and logged.
 */
class FriendlyErrorHandler extends AbstractErrorHandler
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var string[] map where PHP error-level => PSR-3 log-level
     *
     * @link http://php.net/manual/en/errorfunc.constants.php
     */
    private $error_levels = [
        E_NOTICE            => LogLevel::NOTICE,
        E_USER_NOTICE       => LogLevel::NOTICE,
        E_STRICT            => LogLevel::NOTICE,
        E_DEPRECATED        => LogLevel::NOTICE,
        E_USER_DEPRECATED   => LogLevel::NOTICE,
        E_WARNING           => LogLevel::WARNING,
        E_CORE_WARNING      => LogLevel::WARNING,
        E_USER_WARNING      => LogLevel::WARNING,
        E_ERROR             => LogLevel::ERROR,
        E_PARSE             => LogLevel::ERROR,
        E_CORE_ERROR        => LogLevel::ERROR,
        E_COMPILE_ERROR     => LogLevel::ERROR,
        E_COMPILE_WARNING   => LogLevel::ERROR,
        E_USER_ERROR        => LogLevel::ERROR,
        E_RECOVERABLE_ERROR => LogLevel::ERROR,
    ];

    /**
     * @param LoggerInterface $logger logger to which detailed error-information will be logged
     * @param ResponseFactory $factory
     */
    public function __construct(LoggerInterface $logger, ResponseFactory $factory)
    {
        $this->logger = $logger;

        parent::__construct($factory);
    }

    public function handleError(ErrorException $error)
    {
        $severity = $error->getSeverity();

        $level = @$this->error_levels[$severity] ?: LogLevel::ERROR;

        $message = $this->formatError($error);

        $this->logger->log($level, $message);

        if ($level === LogLevel::ERROR) {
            throw $error; // it's dead, Jim!
        }
    }

    public function createErrorResponse(
        ServerRequestInterface $request,
        Throwable $error
    ): ResponseInterface {
        if ($error instanceof ErrorException) {
            // all ErrorExceptions are logged already in handleError() regardless of whether it gets thrown
        } else {
            $this->logger->log(LogLevel::ERROR, $this->formatError($error));
        }

        return parent::createErrorResponse($request, $error);
    }

    /**
     * Internally format log-message from Throwable
     *
     * @param Throwable $error
     *
     * @return string
     */
    private function formatError(Throwable $error): string
    {
        $message = "";

        $prefix = "";

        while ($error) {
            $message .= $prefix . readable::error($error) . "\n\n";
            $message .= readable::trace($error) . "\n\n";

            $error = $error->getPrevious();

            $prefix = "Previous ";
        }

        return $message;
    }

    protected function createErrorPage(ServerRequestInterface $request, Throwable $error): string
    {
        $page = new FriendlyErrorPage($request);

        return $page->render();
    }

    protected function createJSONPayload(ServerRequestInterface $request, Throwable $error): array
    {
        return ['error' => 'Internal Server Error'];
    }

    protected function createPlainText(ServerRequestInterface $request, Throwable $error): string
    {
        $text = "*** 500 Internal Server Error ***\n\n";
        $text .= $request->getMethod() . " " . $request->getUri() . "\n\n";
        $text .= "The error has been logged.\n";

        return $text;
    }
}

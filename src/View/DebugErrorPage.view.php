<?php

use Kodus\Error\View\DebugErrorPage;

/**
 * @var DebugErrorPage $this
 */

?>
<!DOCTYPE html>
<html>
<head>
    <title>500 Internal Server Error</title>
    <style>
        /* overrride inline styles produced by xdebug: */

        .kodus-debug {
            font-size: 14px !important;
            font-family: "Lucida Console", Monaco, "Courier New", Courier, monospace !important;
            font-weight: normal !important;
            cursor: default;
            background: white;
            border-collapse: collapse;
        }

        .kodus-debug th[colspan] {
            background: #ffb200 !important;
            border-color: black;
            white-space: pre; /* break on CR/LF (in stack-traces, etc.) */
        }

        .kodus-debug th[colspan] span {
            display: none; /* hide that ugly icon added by xdebug */
        }

        .kodus-debug th,
        .kodus-debug td {
            margin: 0;
            padding: 2px 6px;
            border: solid 1px #aaa;
        }

        /* display tooltips for abbreviated paths and class-names: */

        .kodus-debug [title] {
            position: relative;
        }

        .kodus-debug [title]:hover {
            color: #c00;
            text-decoration: none;
        }

        .kodus-debug [title]:hover:after {
            background: #111;
            background: rgba(0, 0, 0, .8);
            border-radius: .5em;
            bottom: 1.35em;
            color: #fff;
            content: attr(title);
            display: block;
            left: 1em;
            padding: .3em 1em;
            position: absolute;
            text-shadow: 0 1px 0 #000;
            white-space: nowrap;
            z-index: 98;
        }

        .kodus-debug [title]:hover:before {
            border: solid;
            border-color: #111 transparent;
            border-color: rgba(0, 0, 0, 0.8) transparent;
            border-width: .4em .4em 0 .4em;
            bottom: 1em;
            content: "";
            display: block;
            left: 2em;
            position: absolute;
            z-index: 99;
        }
    </style>
</head>
<body>
<h2>Internal Server Error</h2>
<?= $this->getStackTrace() ?>
</body>
</html>

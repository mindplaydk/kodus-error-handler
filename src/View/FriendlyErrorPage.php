<?php

namespace Kodus\Error\View;

use Kodus\Error\FriendlyErrorHandler;
use Psr\Http\Message\ServerRequestInterface;

/**
 * This view-model represents a user-facing "friendly apology" page, which is displayed
 * by {@see ErrorHandlerMiddleware} if an internal error occurs while processing a request.
 *
 * @see FriendlyErrorHandler
 */
class FriendlyErrorPage extends ErrorPage
{
    /**
     * @var ServerRequestInterface
     */
    public $request;

    /**
     * @param ServerRequestInterface $request
     */
    public function __construct(ServerRequestInterface $request)
    {
        $this->request = $request;
    }
}

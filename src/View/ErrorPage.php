<?php

namespace Kodus\Error\View;

use ReflectionClass;

/**
 * Abstract base-class for self-rendering ErrorPage view-models.
 *
 * The render-method looks for a file with the same name as the class-file,
 * but with a ".view.php" file-extension.
 */
abstract class ErrorPage
{
    public function render(): string
    {
        ob_start();

        require $this->getTemplatePath();

        return ob_get_clean();
    }

    protected function getTemplatePath(): string
    {
        $class = new ReflectionClass($this);

        $path = dirname($class->getFileName());

        $name = $class->getShortName();

        return $path . DIRECTORY_SEPARATOR . $name . ".view.php";
    }
}

<?php

// NOTE: this template is only here to prevent total failure - it should be replaced on production sites.

use Kodus\Error\View\FriendlyErrorPage;

/**
 * @var FriendlyErrorPage $this
 */

?>
<!DOCTYPE html>
<html>
<head>
    <title>500 Internal Server Error</title>
</head>
<body>
<div style="text-align: center">
    <h2>Looks like something went wrong!</h2>
    <h3>We track these errors automatically, but if the problem persists feel free to contact us.
        In the meantime, try refreshing the page.</h3>
    <h3>We apologize for the inconvenience.</h3>
</div>
</body>
</html>

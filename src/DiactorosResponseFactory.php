<?php

namespace Kodus\Error;

use Psr\Http\Message\ResponseInterface;
use Zend\Diactoros\Response;

/**
 * PSR-7 Response Factory for Zend's PSR-7 implementation.
 *
 * If you wish to use this, be sure to require `zendframework/zend-diactoros` in
 * the `composer.json` file of your project.
 *
 * @link https://packagist.org/packages/zendframework/zend-diactoros
 */
class DiactorosResponseFactory implements ResponseFactory
{
    public function createResponse($status = 200): ResponseInterface
    {
        return new Response("php://temp", $status);
    }
}

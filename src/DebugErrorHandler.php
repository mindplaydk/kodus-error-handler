<?php

namespace Kodus\Error;

use ErrorException;
use Kodus\Error\View\ErrorPage;
use Kodus\Error\View\DebugErrorPage;
use mindplay\readable;
use Psr\Http\Message\ServerRequestInterface;
use Throwable;

/**
 * This error-handler is intended for use during development.
 *
 * It will emit an HTML error-page with a stack-trace provided by `xdebug`, where available, a
 * JSON-response, or a plain-text response if the `Accept` header from the client does not
 * indicate a preference for any other response-type.
 *
 * @see DebugErrorPage
 */
class DebugErrorHandler extends AbstractErrorHandler
{
    public function handleError(ErrorException $error)
    {
        throw $error; // all errors will be thrown, even E_STRICT and E_NOTICE - we want you to detect and fix those!
    }

    protected function createErrorPage(ServerRequestInterface $request, Throwable $error): string
    {
        $page = new DebugErrorPage($request, $error);

        return $page->render();
    }

    protected function createJSONPayload(ServerRequestInterface $request, Throwable $error): array
    {
        return $this->toJSON($error);
    }

    protected function createPlainText(ServerRequestInterface $request, Throwable $error): string
    {
        $text = "*** 500 Internal Server Error ***\n\n";
        $text .= $request->getMethod() . " " . $request->getUri() . "\n\n";

        $prefix = "";

        while ($error) {
            $text .= $prefix . readable::error($error) . "\n\n";
            $text .= readable::trace($error) . "\n\n";

            $error = $error->getPrevious();

            $prefix = "Previous ";
        }

        return $text;
    }

    /**
     * @param Throwable $error
     *
     * @return array
     */
    private function toJSON(Throwable $error): array
    {
        $data = [
            'error'   => $error instanceof ErrorException
                ? readable::severity($error->getSeverity())
                : readable::typeof($error),
            'message' => $error->getMessage(),
            'file'    => $error->getFile(),
            'line'    => $error->getLine(),
            'trace'   => readable::trace($error),
        ];

        $previous = $error->getPrevious();

        if ($previous) {
            $data['previous'] = $this->toJSON($previous);
        }

        return $data;
    }
}

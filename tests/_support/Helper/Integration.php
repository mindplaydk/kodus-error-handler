<?php

namespace Helper;

class Integration extends \Codeception\Module
{
    public function assertContentsLike(array $expected, string $actual)
    {
        $pattern = implode(".*", array_map("preg_quote", $expected));

        $this->assertRegExp("/{$pattern}/s", $actual);
    }
}

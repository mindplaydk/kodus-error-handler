<?php

namespace Kodus\Error\Test\Integration;

use Exception;
use IntegrationTester;
use Kodus\Error\DebugErrorHandler;
use Kodus\Error\DiactorosResponseFactory;
use Kodus\Error\ErrorHandler;

/**
 * Integration test for plain-text and custom HTML stack-traces from the debug error-handler.
 *
 * NOTE: this deliberate does NOT include a test for the JSON response, which is covered by `KodusClientErrorCest`
 *
 * @see KodusClientErrorCest
 */
class DebugErrorHandlerCest extends ErrorHandlerCestBase
{
    protected function createErrorHandler(): ErrorHandler
    {
        return new DebugErrorHandler(new DiactorosResponseFactory());
    }

    public function createTextResponse(IntegrationTester $I)
    {
        $response = $this->createResponse(new Exception("oh dear!"), ["Accept" => "text/plain"]);

        $I->assertSame("text/plain", $response->getHeaderLine("Content-Type"));

        $I->assertContentsLike(
            [
                "RuntimeException with message: oh noes! in",
                "TestMiddleware.php",
                "1.",
                "TestMiddleware.php",
                "Kodus\\Error\\Test\\Integration\\TestMiddleware->firstMethod()",
                "Previous Exception with message: oh dear! in",
                "DebugErrorHandlerCest.php"
            ],
            $response->getBody()
        );
    }

    public function createHTMLResponse(IntegrationTester $I)
    {
        $xdebug_enabled = $this->isXDebugEnabled();

        if ($xdebug_enabled) {
            $this->disableXDebug(); // allows us to test our custom stack-trace rendering facility
        }

        $response = $this->createResponse(new Exception("oh dear me!"), ["Accept" => "text/html"]);

        $I->assertSame("text/html; charset=utf-8", $response->getHeaderLine("Content-Type"));

        $I->assertContentsLike(
            [
                "RuntimeException with message: oh noes! in",
                "TestMiddleware.php",
                "1",
                "TestMiddleware->firstMethod()",
                "TestMiddleware.php",
                "Previous Exception with message: oh dear me! in",
                "DebugErrorHandlerCest.php"
            ],
            html_entity_decode(strip_tags($response->getBody()))
        );

        if ($xdebug_enabled) {
            $this->enableXDebug();
        }
    }

    public function createJSONResponse(IntegrationTester $I)
    {
        $response = $this->createResponse(new Exception(), ["Accept" => "application/json"]);

        $I->assertSame("application/json", $response->getHeaderLine("Content-Type"));
    }


    public function cleanOutputBuffers(IntegrationTester $I)
    {
        $level = ob_get_level();

        $response = $this->createHangingResponse();

        $I->assertSame($level, ob_get_level(), "error-handler cleans up hanging output buffers");

        $I->assertSame(500, $response->getStatusCode());

        $I->assertRegExp("/unexpected buffer-level/", (string) $response->getBody());
    }

    public function cleanOutputBuffersAfterException(IntegrationTester $I)
    {
        $level = ob_get_level();

        $response = $this->createFailingResponse();

        $I->assertSame($level, ob_get_level(), "error-handler cleans up hanging output buffers after exception");

        $I->assertSame(500, $response->getStatusCode());

        $class_name = preg_quote(FailingOutputBufferMiddleware::class);

        $I->assertRegExp("/from {$class_name}/", (string) $response->getBody());
    }
}

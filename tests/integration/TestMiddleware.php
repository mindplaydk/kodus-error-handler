<?php

namespace Kodus\Error\Test\Integration;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use RuntimeException;
use Throwable;
use Zend\Diactoros\Response;

class TestMiddleware implements MiddlewareInterface
{
    /**
     * @var int|Throwable|null
     */
    protected $error;

    /**
     * @param int|Throwable|null $error
     */
    public function __construct($error)
    {
        $this->error = $error;
    }

    public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $this->firstMethod();

        $response = new Response("php://memory", 200);

        $response->getBody()->write("OK");

        return $response;
    }

    private function firstMethod()
    {
        try {
            $this->secondMethod();
        } catch (Throwable $error) {
            throw new RuntimeException("oh noes!", 123, $error);
        }
    }

    private function secondMethod()
    {
        if (is_int($this->error)) {
            trigger_error("ERROR", $this->error);
        }

        if ($this->error instanceof Throwable) {
            throw $this->error;
        }
    }
}

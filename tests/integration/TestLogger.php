<?php

namespace Kodus\Error\Test\Integration;

use Psr\Log\AbstractLogger;

class TestLogger extends AbstractLogger
{
    /**
     * @var string[]
     */
    public $records = [];

    public function log($level, $message, array $context = [])
    {
        $this->records[] = $message;
    }
}

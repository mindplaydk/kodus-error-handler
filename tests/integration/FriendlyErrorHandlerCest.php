<?php

namespace Kodus\Error\Test\Integration;

use Exception;
use IntegrationTester;
use Kodus\Error\DiactorosResponseFactory;
use Kodus\Error\ErrorHandler;
use Kodus\Error\FriendlyErrorHandler;

class FriendlyErrorHandlerCest extends ErrorHandlerCestBase
{
    /**
     * @var TestLogger
     */
    protected $logger;

    public function _before(IntegrationTester $I)
    {
        $this->logger = new TestLogger();
    }

    protected function createErrorHandler(): ErrorHandler
    {
        return new FriendlyErrorHandler($this->logger, new DiactorosResponseFactory());
    }

    public function createTextResponse(IntegrationTester $I)
    {
        $response = $this->createResponse(new Exception("oops!"));

        $I->assertSame("text/plain", $response->getHeaderLine("Content-Type"));

        $this->checkLog($I);
    }

    public function createHTMLResponse(IntegrationTester $I)
    {
        $response = $this->createResponse(new Exception("oops!"), ["Accept" => "text/html"]);

        $I->assertSame("text/html; charset=utf-8", $response->getHeaderLine("Content-Type"));

        $this->checkLog($I);
    }

    public function createJSONResponse(IntegrationTester $I)
    {
        $response = $this->createResponse(new Exception("oops!"), ["Accept" => "application/json"]);

        $I->assertSame("application/json", $response->getHeaderLine("Content-Type"));

        $this->checkLog($I);
    }

    protected function checkLog(IntegrationTester $I)
    {
        $I->assertCount(1, $this->logger->records);

        $I->assertContentsLike(
            [
                "RuntimeException with message: oh noes! in",
                "TestMiddleware.php",
                "1.",
                "TestMiddleware.php",
                "Kodus\\Error\\Test\\Integration\\TestMiddleware->firstMethod()",
                "Previous Exception with message: oops! in",
                "FriendlyErrorHandlerCest.php"
            ],
            $this->logger->records[0]
        );
    }

    public function cleanOutputBuffers(IntegrationTester $I)
    {
        $level = ob_get_level();

        $response = $this->createHangingResponse();

        $I->assertSame($level, ob_get_level(), "error-handler cleans up hanging output buffers");

        $I->assertSame(500, $response->getStatusCode());

        $I->assertRegExp("/unexpected buffer-level/", $this->logger->records[0]);
    }

    public function cleanOutputBuffersAfterException(IntegrationTester $I)
    {
        $level = ob_get_level();

        $response = $this->createFailingResponse();

        $I->assertSame($level, ob_get_level(), "error-handler cleans up hanging output buffers after exception");

        $I->assertSame(500, $response->getStatusCode());

        $class_name = preg_quote(FailingOutputBufferMiddleware::class);

        $I->assertRegExp("/from {$class_name}/", $this->logger->records[0]);
    }
}

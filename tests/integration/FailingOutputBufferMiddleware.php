<?php

namespace Kodus\Error\Test\Integration;

use Exception;
use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;

class FailingOutputBufferMiddleware implements MiddlewareInterface
{
    public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        ob_start(); // leave a hanging output buffer for the error-handler to detect

        throw new Exception("from " . self::class);
    }
}

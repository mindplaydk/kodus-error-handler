<?php

namespace Kodus\Error\Test\Integration;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\TextResponse;

class HangingOutputBufferMiddleware implements MiddlewareInterface
{
    public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        ob_start(); // leave a hanging output buffer for the error-handler to detect

        return new TextResponse("left an open output-buffer");
    }
}

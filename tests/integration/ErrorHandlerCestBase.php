<?php

namespace Kodus\Error\Test\Integration;

use IntegrationTester;
use Kodus\Error\ErrorHandler;
use Kodus\Error\ErrorHandlerMiddleware;
use mindplay\middleman\Dispatcher;
use Psr\Http\Message\ResponseInterface;
use RuntimeException;
use Throwable;
use Zend\Diactoros\ServerRequest;

abstract class ErrorHandlerCestBase
{
    public function createInternalServerError(IntegrationTester $I)
    {
        $I->assertSame(500, $this->createResponse(E_USER_ERROR)->getStatusCode(), "handles E_USER_ERROR");
        $I->assertSame(500, $this->createResponse(new RuntimeException())->getStatusCode(), "handles RuntimeException");
    }

    public function createSuccessfulResponse(IntegrationTester $I)
    {
        $response = $this->createResponse();

        $I->assertSame(200, $response->getStatusCode());
        $I->assertSame("OK", (string) $response->getBody());
    }

    public function cleanHangingOutputBuffers(IntegrationTester $I)
    {
        $dispatcher = new Dispatcher([
            new ErrorHandlerMiddleware($this->createErrorHandler()),
            new FailingOutputBufferMiddleware(),
        ]);

        $ob_level = ob_get_level();

        $dispatcher->dispatch(new ServerRequest([], [], "/", "GET"));

        $I->assertSame($ob_level, ob_get_level(), "should clean hanging output buffers");
    }

    abstract protected function createErrorHandler(): ErrorHandler;

    /**
     * @param int|Throwable|null $error
     * @param string[]           $headers
     *
     * @return ResponseInterface
     */
    protected function createResponse($error = null, $headers = []): ResponseInterface
    {
        $dispatcher = new Dispatcher([
            new ErrorHandlerMiddleware($this->createErrorHandler()),
            new TestMiddleware($error),
        ]);

        return $dispatcher->dispatch(new ServerRequest([], [], "/", "GET", "php://temp", $headers));
    }

    protected function createFailingResponse(): ResponseInterface
    {
        $dispatcher = new Dispatcher([
            new ErrorHandlerMiddleware($this->createErrorHandler()),
            new FailingOutputBufferMiddleware(),
        ]);

        return $dispatcher->dispatch(new ServerRequest([], [], "/", "GET", "php://temp"));
    }

    protected function createHangingResponse(): ResponseInterface
    {
        $dispatcher = new Dispatcher([
            new ErrorHandlerMiddleware($this->createErrorHandler()),
            new HangingOutputBufferMiddleware(),
        ]);

        return $dispatcher->dispatch(new ServerRequest([], [], "/", "GET", "php://temp"));
    }

    protected function isXDebugEnabled(): bool
    {
        return function_exists("xdebug_is_enabled")
            ? xdebug_is_enabled()
            : false;
    }

    protected function disableXDebug()
    {
        if (function_exists("xdebug_disable")) {
            xdebug_disable();
        }
    }

    protected function enableXDebug()
    {
        if (function_exists("xdebug_enable")) {
            xdebug_enable();
        }
    }
}
